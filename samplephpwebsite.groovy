job('samplephpwebsite-v1') {
    description 'samplephpwebsite-v1'
    scm {
        git {
            remote {
                name('origin')
                url('https://gitlab.com/Kiiero/eval-project.git')
            }
            branch('v1')
            }
    }
    triggers{
        scm('H/30 * * * *')
    }
    steps{
        dockerBuildAndPublish {
            repositoryName('kiiero/nginx-php-fpm:1.0')
            registryCredentials('kiieroeval')
			buildContext('nginx-php-fpm')
            forcePull(false)
            createFingerprints(false)
            skipDecorate()
        }
    }
}

job('samplephpwebsite-v2') {
    description 'samplephpwebsite-v2'
    scm {
        git {
            remote {
                name('origin')
                url('https://gitlab.com/Kiiero/eval-project.git')
            }
            branch('v2')
            }
    }
    triggers{
        scm('H/30 * * * *')
    }
    steps{
        dockerBuildAndPublish {
            repositoryName('kiiero/nginx-php-fpm:2.0')
            registryCredentials('kiieroeval')
			buildContext('nginx-php-fpm')
            forcePull(false)
            createFingerprints(false)
            skipDecorate()
        }
    }
}

job('samplephpwebsite-v3') {
    description 'samplephpwebsite-v3'
    scm {
        git {
            remote {
                name('origin')
                url('https://gitlab.com/Kiiero/eval-project.git')
            }
            branch('v3')
            }
    }
    triggers{
        scm('H/30 * * * *')
    }
    steps{
        dockerBuildAndPublish {
            repositoryName('kiiero/nginx-php-fpm:3.0')
            registryCredentials('kiieroeval')
			buildContext('nginx-php-fpm')
            forcePull(false)
            createFingerprints(false)
            skipDecorate()
        }
    }
}
